# MigrateU #
___
### What is this repository for? ###

* This will be used to assist with and complete migrations on a cPanel server for the (mt) Media Temple CloudTech team.
* Currently supported CMS applications:
    1 WordPress
    2 Joomla
    3 Drupal
    4 Magento
    5 Zencart
* Version: 0.5

### How do I get set up? ###

* Wget the file and execute it from below the user's home on the file system (/home/username/...) and make it executable.

* Available options:

        Usage: osmosis options [-s source_dir] [-d destinaton_dir] [-f] [-l [-u username] ] [-h]

        -s </path/to/source_dir/> : path to source directory (relative and absolute paths are fine).
        -d </path/to/dest_dir/> : path to destination directory (relative and absolute paths are fine).
        -f : skip backing up the files in the destination directory.
        -l : list domains and document roots for user. Without the -u [username] option, you must be lower then the user's home directory on the file system (/home/username/...)
        -u <username> : Only used when the -l option is selected in order to specify a user.
		-S <domain.com> : Source domain.
		-D <new.domain.com> : Destination domain
        -h : Output this helpful guide.

 ** Example usage: **

  1  `./osmosis -l -u testuser`

  2  `./osmosis -s domain.com/files/ -d ../../public_html/ -S domain.com -D new.domain.com`


### Authors ###
* Fabian Barajas
* Gary Robert

### Contributor ###
* David Kobayashi